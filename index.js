const express = require('express')
const app = express()
const port = 3000

const STUDENTS = ["Mohamed", "Ahmed", "Ali", "Farag", "Abdallah"];

app.get('/students', (req, res) => {
  let result = `<ul> ${STUDENTS.map(name => `<li>${name}</li>`).join('')} </ul>`
  res.send(result)
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
